﻿using NETProjectTutorial.implements;
using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> ListProducto = new List<Producto>();
        private implements.DaoImplementsProducto daoProducto;

        public ProductoModel()
        {
            daoProducto = new implements.DaoImplementsProducto();
        }

        public List<Producto> GetListProducto()
        {
            return daoProducto.findAll();
        }

        public void Populate()
        {
            ListProducto = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));

            foreach (Producto p in ListProducto)
            {
                daoProducto.save(p);
            }
        }

        public void save(DataRow prod)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(prod["Id"].ToString());
            p.Sku = prod["SKU"].ToString();
            p.Nombre = prod["Nombre"].ToString();
            p.Descripcion = prod["Descripción"].ToString();
            p.Cantidad = Convert.ToInt32(prod["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(prod["Precio"].ToString());

            daoProducto.save(p);
        }

        public void update(DataRow prod)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(prod["Id"].ToString());
            p.Sku = prod["SKU"].ToString();
            p.Nombre = prod["Nombre"].ToString();
            p.Descripcion = prod["Descripción"].ToString();
            p.Cantidad = Convert.ToInt32(prod["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(prod["Precio"].ToString());

            daoProducto.update(p);
        }

        public void delete(DataRow prod)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(prod["Id"].ToString());
            p.Sku = prod["SKU"].ToString();
            p.Nombre = prod["Nombre"].ToString();
            p.Descripcion = prod["Descripción"].ToString();
            p.Cantidad = Convert.ToInt32(prod["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(prod["Precio"].ToString());

            daoProducto.delete(p);
        }
    }
}
