﻿using NETProjectTutorial.entities;
using NETProjectTutorial.implements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> ListCliente = new List<Cliente>();
        private implements.DaoImplementsCliente daoCliente;

        public ClienteModel()
        {
            daoCliente = new implements.DaoImplementsCliente();
        }

        public List<Cliente> GetListCliente()
        {
            return daoCliente.findAll();
        }

        public void Populate()
        {
            ListCliente = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Cliente_data));

            foreach (Cliente c in ListCliente)
            {
                daoCliente.save(c);
            }
        }

        public void save(DataRow clie)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(clie["Id"].ToString());
            c.Cedula = clie["Cédula"].ToString();
            c.Nombres = clie["Nombre"].ToString();
            c.Apellidos = clie["Apellidos"].ToString();
            c.Telefono = clie["Teléfono"].ToString();
            c.Correo = clie["Correo"].ToString();
            c.Direccion = clie["Dirección"].ToString();

            daoCliente.save(c);
        }

        public void update(DataRow clie)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(clie["Id"].ToString());
            c.Cedula = clie["Cédula"].ToString();
            c.Nombres = clie["Nombre"].ToString();
            c.Apellidos = clie["Apellidos"].ToString();
            c.Telefono = clie["Teléfono"].ToString();
            c.Correo = clie["Correo"].ToString();
            c.Direccion = clie["Dirección"].ToString();

            daoCliente.update(c);
        }

        public void delete(DataRow clie)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(clie["Id"].ToString());
            c.Cedula = clie["Cédula"].ToString();
            c.Nombres = clie["Nombre"].ToString();
            c.Apellidos = clie["Apellidos"].ToString();
            c.Telefono = clie["Teléfono"].ToString();
            c.Correo = clie["Correo"].ToString();
            c.Direccion = clie["Dirección"].ToString();

            daoCliente.delete(c);
        }
    }
}
