﻿using NETProjectTutorial.dao;
using NETProjectTutorial.entities;
using NETProjectTutorial.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial.implements
{
    class DaoImplementsCliente : IDaoCliente
    {
        //header cliente
        private BinaryReader brhcliente;
        private BinaryWriter bwhcliente;
        //data cliente
        private BinaryReader brdcliente;
        private BinaryWriter bwdcliente;

        private FileStream fshcliente;
        private FileStream fsdcliente;
        private const string FILENAME_HEADER = "hcliente.dat";
        private const string FILENAME_DATA = "dcliente.dat";
        private const int SIZE = 390;

        public DaoImplementsCliente()
        {
            RandomFileBinarySearch.SIZE = 4;    
        }

        private void open()
        {
            try
            {
                fsdcliente = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                if (!File.Exists(FILENAME_HEADER))
                {
                    fshcliente = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhcliente = new BinaryReader(fshcliente);
                    bwhcliente = new BinaryWriter(fshcliente);
                    brdcliente = new BinaryReader(fsdcliente);
                    bwdcliente = new BinaryWriter(fsdcliente);
                    bwhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhcliente.Write(0); //n
                    bwhcliente.Write(0); //k
                } else
                {
                    fshcliente = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhcliente = new BinaryReader(fshcliente);
                    bwhcliente = new BinaryWriter(fshcliente);
                    brdcliente = new BinaryReader(fsdcliente);
                    bwdcliente = new BinaryWriter(fsdcliente);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            try
            {
                if(brdcliente != null)
                {
                    brdcliente.Close();
                }
                if(brhcliente != null)
                {
                    brhcliente.Close();
                }
                if(bwdcliente != null)
                {
                    bwdcliente.Close();
                }
                if(brdcliente != null)
                {
                    brdcliente.Close();
                }
                if(fsdcliente != null)
                {
                    fsdcliente.Close();
                }
                if(fshcliente != null)
                {
                    fshcliente.Close();
                }
            }
            catch (IOException e)
            {

                throw new  IOException(e.Message);
            }
        }

        public bool delete(Cliente t)
        {
            /*try
            {
                FileStream temp = new FileStream("tmp.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                BinaryReader tempbrhcliente = new BinaryReader(temp);
                BinaryWriter tempbwhcliente = new BinaryWriter(temp);
                open();
                brhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brhempleado.ReadInt32();
                int k = brhempleado.ReadInt32();

                tempbrhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
                tempbwhcliente.Write(n - 1);
                tempbwhcliente.Write(k);
                int j = 0;

                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + 4 * i;
                    brhempleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
                    int id = brhempleado.ReadInt32();
                    if (id == t.Id)
                    {
                        continue;
                    }
                    long tmpos = 8 + 4 * j++;
                    tempbrhcliente.BaseStream.Seek(tmpos, SeekOrigin.Begin);
                    tempbwhcliente.Write(id);
                }

                close();
                File.Delete(FILENAME_HEADER);
                bool flag = File.Exists(FILENAME_HEADER);

                if (flag)
                {
                    File.Move(temp.Name, FILENAME_HEADER);
                } else
                {
                    DialogResult result;
                    result = MessageBox.Show("No se pudo eliminar el registro", 
                        "ERROR", MessageBoxButtons.OK);
                }
                return flag;
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }*/

            if (t == null) return false;

            try
            {
                open();

                brhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
                List<int> temp = new List<int>();
                int n = brhcliente.ReadInt32();
                int k = brhcliente.ReadInt32();
                for (int i = 0; i < n; i++)
                {
                    temp.Add(brhcliente.ReadInt32());
                }
                temp.Remove(temp.BinarySearch(t.Id));

                bwhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
                bwhcliente.Write(--n);
                bwhcliente.Write(k);

                foreach (int i in temp) bwhcliente.Write(i);

                close();
            }
            catch (IOException)
            {
                return false;
            }
            return true;
        }

        public List<Cliente> findAll()
        {
            open();
            List<Cliente> clientes = new List<Cliente>();

            brhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhcliente.ReadInt32();

            for(int i=0; i<n; i++)
            {
                //Calculamos posición cabecera
                long hpos = 8 + i * 4;
                brhcliente.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhcliente.ReadInt32();

                //Calculamos posición de los datos
                long dpos = (index - 1) * SIZE;
                brdcliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdcliente.ReadInt32();
                string cedula = brdcliente.ReadString();
                string nombres = brdcliente.ReadString();
                string apellidos = brdcliente.ReadString();
                string telefono = brdcliente.ReadString();
                string correo = brdcliente.ReadString();
                string direccion = brdcliente.ReadString();
                Cliente c = new Cliente(id, cedula, nombres, apellidos, telefono, correo, direccion);
                clientes.Add(c);
            }
            
            close();
            return clientes;
        }

        public Cliente findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public Cliente findById(string Id)
        {
            throw new NotImplementedException();
        }

        public List<Cliente> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Cliente t)
        {
            open();
            brhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhcliente.ReadInt32();
            int k = brhcliente.ReadInt32();

            long dpos = k * SIZE;
            bwdcliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdcliente.Write(++k);
            bwdcliente.Write(t.Cedula);
            bwdcliente.Write(t.Nombres);
            bwdcliente.Write(t.Apellidos);
            bwdcliente.Write(t.Telefono);
            bwdcliente.Write(t.Correo);
            bwdcliente.Write(t.Direccion);

            bwhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhcliente.Write(++n);
            bwhcliente.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhcliente.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhcliente.Write(k);
            close();
        }

        public int update(Cliente t)
        {
            open();
            brhcliente.BaseStream.Seek(0, SeekOrigin.Begin);
            RandomFileBinarySearch.brhcliente = brhcliente;
            int n = brhcliente.ReadInt32();
            int pos = RandomFileBinarySearch.runBinarySearchRecursively(t.Id, 0, n);
            if (pos < 0) 
            {
                close();
                return pos;
            }

            long hpos = 8 + 4 * (pos);
            brhcliente.BaseStream.Seek(hpos, SeekOrigin.Begin);
            int id = brhcliente.ReadInt32();

            long dpos = (id - 1) * SIZE;
            brdcliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdcliente.Write(t.Id);
            bwdcliente.Write(t.Cedula);
            bwdcliente.Write(t.Nombres);
            bwdcliente.Write(t.Apellidos);
            bwdcliente.Write(t.Telefono);
            bwdcliente.Write(t.Correo);
            bwdcliente.Write(t.Direccion);

            close();
            return t.Id;
        }
    }
}
