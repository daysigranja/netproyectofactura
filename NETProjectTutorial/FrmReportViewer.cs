﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReportViewer : Form
    {
        private DataSet dsSistema;
        private DataRow drFEmpleado;

        public DataSet DsSistema
        {
            get
            {
                return dsSistema;
            }

            set
            {
                dsSistema = value;
            }
        }

        public DataRow DrFEmpleado
        {
            get
            {
                return drFEmpleado;
            }

            set
            {
                drFEmpleado = value;
            }
        }

        public FrmReportViewer()
        {
            InitializeComponent();
        }

        private void FrmReportViewer_Load(object sender, EventArgs e)
        {
            dsSistema.Tables["ReportFacturaEmpleado"].Rows.Clear();
            
        }
    }
}
