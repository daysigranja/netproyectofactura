﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        //Factura
        private string cod_factura;
        private DateTime fecha;
        private int id_empleado;        
        private double subtotal;
        private double iva;
        private double total;

        //DetalleFactura
        private int id_factura;
        private int id_producto;
        private int cantidad;
        private double precio;

        //Producto
        private string sku;
        private string nombre_producto;

        //Empleado
        private string nombre_empleado;
        private string apellido_empleado;

        //Cliente
        private string nombre_cliente;
        private string apellido_cliente;

        public ReporteFactura(string cod_factura, DateTime fecha, int id_empleado, double subtotal, double iva, double total, int id_factura, int id_producto, int cantidad, double precio, string sku, string nombre_producto, string nombre_empleado, string apellido_empleado, string nombre_cliente, string apellido_cliente)
        {
            this.cod_factura = cod_factura;
            this.fecha = fecha;
            this.id_empleado = id_empleado;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
            this.id_factura = id_factura;
            this.id_producto = id_producto;
            this.cantidad = cantidad;
            this.precio = precio;
            this.sku = sku;
            this.nombre_producto = nombre_producto;
            this.nombre_empleado = nombre_empleado;
            this.apellido_empleado = apellido_empleado;
            this.nombre_cliente = nombre_cliente;
            this.apellido_cliente = apellido_cliente;
        }

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public int Id_empleado
        {
            get
            {
                return id_empleado;
            }

            set
            {
                id_empleado = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Id_factura
        {
            get
            {
                return id_factura;
            }

            set
            {
                id_factura = value;
            }
        }

        public int Id_producto
        {
            get
            {
                return id_producto;
            }

            set
            {
                id_producto = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombre_producto
        {
            get
            {
                return nombre_producto;
            }

            set
            {
                nombre_producto = value;
            }
        }

        public string Nombre_empleado
        {
            get
            {
                return nombre_empleado;
            }

            set
            {
                nombre_empleado = value;
            }
        }

        public string Apellido_empleado
        {
            get
            {
                return apellido_empleado;
            }

            set
            {
                apellido_empleado = value;
            }
        }

        public string Nombre_cliente { get => nombre_cliente; set => nombre_cliente = value; }
        public string Apellido_cliente { get => apellido_cliente; set => apellido_cliente = value; }
    }
}
