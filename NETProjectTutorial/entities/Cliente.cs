﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Cliente
    {
        private int id; //1 => 4
        private string cedula; //16 => 35
        private string nombres; //20 => 43
        private string apellidos; //20 => 43
        private string telefono; //8 => 19
        private string correo; //20 => 43
        private string direccion; //100 => 203
                                               //Total => 390

        public Cliente() { }

        public Cliente(int id, string cedula, string nombres, string apellidos, string telefono, string correo, string direccion)
        {
            this.id = id;
            this.cedula = cedula;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.telefono = telefono;
            this.correo = correo;
            this.direccion = direccion;
        }

        public int Id { get => id; set => id = value; }
        public string Cedula { get => cedula; set => cedula = value; }
        public string Nombres { get => nombres; set => nombres = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Correo { get => correo; set => correo = value; }
        public string Direccion { get => direccion; set => direccion = value; }
    }
}
