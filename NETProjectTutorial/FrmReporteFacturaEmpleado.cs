﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporteFacturaEmpleado : Form
    {

        private DataSet dsFEmpleado;
        private BindingSource bsFEmpleado;

        public DataSet DsSistema
        {
            get
            {
                return dsFEmpleado;
            }

            set
            {
                dsFEmpleado = value;
            }
        }


        public FrmReporteFacturaEmpleado()
        {
            InitializeComponent();
            bsFEmpleado = new BindingSource();
        }

        private void FrmReporteFacturaEmpleado_Load(object sender, EventArgs e)
        {
            bsFEmpleado.DataSource = dsFEmpleado;
            bsFEmpleado.DataMember = dsFEmpleado.Tables["Facturas"].TableName;
            dgvFEmpleado.DataSource = bsFEmpleado;
            dgvFEmpleado.AutoGenerateColumns = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmReportViewer frvFEmpleado = new FrmReportViewer();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
