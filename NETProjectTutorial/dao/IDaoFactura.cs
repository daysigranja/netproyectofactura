﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoFactura : IDao<Factura>
    {
        Factura findById(string Id);
        Factura findByCodigoFactura(string cod_factura);
    }
}
