﻿using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleados : Form
    {
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;
        private EmpleadoModel empleadoModel;
        DataTable dtEmpleadosDeletedCopy;

        public DataSet DsEmpleados
        {
            get
            {
                return dsEmpleados;
            }

            set
            {
                dsEmpleados = value;
            }
        }

        public FrmGestionEmpleados()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
            empleadoModel = new EmpleadoModel();
        }

        private void TxtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsEmpleados.Filter = string.Format("Cédula like '*{0}*' or Nombre like '*{0}*' or Apellidos like '*{0}*'", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void FrmGestionEmpleados_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = DsEmpleados;
            bsEmpleados.DataMember = DsEmpleados.Tables["Empleado"].TableName;
            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;

            dtEmpleadosDeletedCopy = dsEmpleados.Tables["Empleado"].Clone();
        }


        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmEmpleado fp = new FrmEmpleado(); 
            fp.TblEmpleados = DsEmpleados.Tables["Empleado"];
            fp.DsEmpleados = DsEmpleados;
            fp.ShowDialog();    
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmEmpleado fp = new FrmEmpleado();
            fp.TblEmpleados = DsEmpleados.Tables["Producto"];
            fp.DsEmpleados = DsEmpleados;
            fp.DrEmpleados = drow;
            fp.ShowDialog();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder eliminar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "¿Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            DataRow copyDrow = dtEmpleadosDeletedCopy.NewRow();

            if (result == DialogResult.Yes)
            {
                copyDrow.ItemArray = drow.ItemArray;
                DsEmpleados.Tables["Empleado"].Rows.Remove(drow);
                dtEmpleadosDeletedCopy.Rows.Add(copyDrow);

                MessageBox.Show(this, "Registro eliminado satisfactoriamente", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void FrmGestionEmpleados_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataTable dtEmpleadosAdded = dsEmpleados.Tables["Empleado"].GetChanges(DataRowState.Added);
            DataTable dtEmpleadosUpdated = dsEmpleados.Tables["Empleado"].GetChanges(DataRowState.Modified);
            DataTable dtEmpleadosDeleted = dsEmpleados.Tables["Empleado"].GetChanges(DataRowState.Deleted);

            if (dtEmpleadosAdded != null)
            {
                foreach (DataRow dr in dtEmpleadosAdded.Rows)
                {
                    empleadoModel.save(dr);
                    dsEmpleados.Tables["Empleado"].Rows.Find(dr["Id"]).AcceptChanges();
                }
            }

            if (dtEmpleadosUpdated != null)
            {
                foreach (DataRow dr in dtEmpleadosUpdated.Rows)
                {
                    empleadoModel.update(dr);
                    dsEmpleados.Tables["Empleado"].Rows.Find(dr["Id"]).AcceptChanges();
                }
            }

            if (dtEmpleadosDeletedCopy.Rows.Count > 0)
            {
                foreach (DataRow dr in dtEmpleadosDeletedCopy.Rows)
                {
                    empleadoModel.delete(dr);
                }
            }

        }
    }
}
