﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos frmgp = new FrmGestionProductos();
            frmgp.MdiParent = this;
            frmgp.DsProductos = dsProductos;
            frmgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            //new ProductoModel().Populate();
            foreach (Producto producto in new ProductoModel().GetListProducto())
            {
                DataRow drProducto = dsProductos.Tables["Producto"].NewRow();
                drProducto["Id"] = producto.Id;
                drProducto["SKU"] = producto.Sku;
                drProducto["Nombre"] = producto.Nombre;
                drProducto["Descripcion"] = producto.Descripcion;
                drProducto["Cantidad"] = producto.Cantidad;
                drProducto["Precio"] = producto.Precio;
                dsProductos.Tables["Producto"].Rows.Add(drProducto);
                drProducto.AcceptChanges();
            }

            //new EmpleadoModel().Populate();
            /*foreach (Empleado employee in new EmpleadoModel().GetListEmpleado())
            {
                DataRow drEmpleado = dsProductos.Tables["Empleado"].NewRow();
                drEmpleado["Id"] = employee.Id;
                drEmpleado["INSS"] = employee.Inss;
                drEmpleado["Cédula"] = employee.Cedula;
                drEmpleado["Nombre"] = employee.Nombre;
                drEmpleado["Apellido"] = employee.Apellidos;
                drEmpleado["Direccion"] = employee.Direccion;
                drEmpleado["Teléfono"] = employee.Tconvencional;
                drEmpleado["Celular"] = employee.Tcelular;
                drEmpleado["Salario"] = employee.Salario;
                drEmpleado["Sexo"] = employee.Sexo;
                dsProductos.Tables["Empleado"].Rows.Add(drEmpleado);
                drEmpleado.AcceptChanges();

            }*/

            //new ClienteModel().Populate();
            foreach (Cliente cliente in new ClienteModel().GetListCliente())
            {
                DataRow drCliente = dsProductos.Tables["Cliente"].NewRow();
                drCliente["Id"] = cliente.Id;
                drCliente["Cédula"] = cliente.Cedula;
                drCliente["Nombre"] = cliente.Nombres;
                drCliente["Apellidos"] = cliente.Apellidos;
                drCliente["Teléfono"] = cliente.Telefono;
                drCliente["Correo"] = cliente.Correo;
                drCliente["Dirección"] = cliente.Direccion;
                dsProductos.Tables["Cliente"].Rows.Add(drCliente);
                drCliente.AcceptChanges();
            }
        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados frmge = new FrmGestionEmpleados();
            frmge.MdiParent = this;
            frmge.DsEmpleados = dsProductos;
            frmge.Show();
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura frmf = new FrmFactura();
            frmf.MdiParent = this;
            frmf.DsSistema = dsProductos;
            frmf.Show();
        }

        private void FacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionFacturas frmgf = new FrmGestionFacturas();
            frmgf.MdiParent = this;
            frmgf.DsFacturas = dsProductos;
            frmgf.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes frmgc = new FrmGestionClientes();
            frmgc.MdiParent = this;
            frmgc.DsClientes = dsProductos;
            frmgc.Show();
        }

        private void facturaciònPorEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmReporteFacturaEmpleado frFE = new FrmReporteFacturaEmpleado();
            frFE.MdiParent = this;
            frFE.DsSistema = dsProductos;
            frFE.Show();
        }
    }
}
